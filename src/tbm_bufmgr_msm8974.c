/**************************************************************************
 * tbm_bufmgr_msm8974.c
 *
 * Copyright (c) 2009, 2013 The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of The Linux Foundation nor
 *       the names of its contributors may be used to endorse or promote
 *       products derived from this software without specific prior written
 *       permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NON-INFRINGEMENT ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */


#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>

#include <tbm_bufmgr.h>
#include <tbm_bufmgr_backend.h>
#include <xf86drm.h>

#include <drm/kgsl_drm.h>

#ifdef USE_GENLOCK
#include "genlock.h"
#endif

#define DEBUG
#define GENLOCK_FD

//dlog not part of build system
#undef DEBUG

#ifdef DEBUG
#define LOG_TAG	"TBM_BACKEND"
#include <dlog.h>
static int bDebug = 0;
#define DBG(...) if(bDebug&0x1) LOGD(__VA_ARGS__)
#else
#define DBG(...)
#endif

/* check condition */
#define MSM_RETURN_IF_FAIL(cond) {\
    if (!(cond)) {\
        fprintf (stderr, "[%s] : '%s' failed.\n", __FUNCTION__, #cond);\
        return;\
    }\
}
#define MSM_RETURN_VAL_IF_FAIL(cond, val) {\
    if (!(cond)) {\
        fprintf (stderr, "[%s] : '%s' failed.\n", __FUNCTION__, #cond);\
        return val;\
    }\
}

#define MSM_DRM_MEMTYPE_EBI          0
#define MSM_DRM_MEMTYPE_SMI          1
#define MSM_DRM_MEMTYPE_KMEM         2
#define MSM_DRM_MEMTYPE_KMEM_NOCACHE 3
#define MSM_DRM_MEMTYPE_SECURE         4


#ifdef USE_CACHE
//TBM OPTIONS FOR CACHE STATE
#define TBM_MSM8974_IS_NONE         (0)    // set default value
#define TBM_MSM8974_IS_DIRTY        (1<<0) // set this bit if buffer is dirtied/accessed by CPU
#define TBM_MSM8974_IS_INVALID      (1<<1) // set this bit if buffer was accessed by GPU


#define BO_IS_CACHEABLE(bo_msm) ((bo_msm->flags & TBM_BO_NONCACHABLE)?0:1)
#define TBM_MSM8974_IS_CACHE_DIRTY(cache) ((cache & TBM_MSM8974_IS_DIRTY)?(1):(0))
#define TBM_MSM8974_IS_CACHE_INV(cache) ((cache & TBM_MSM8974_IS_INVALID)?(1):(0))
#define TBM_MSM8974_IS_FLUSH_INV(cache) ((cache & TBM_CACHE_INV)?(1):(0))
#define TBM_MSM8974_IS_FLUSH_CLN(cache) ((cache & TBM_CACHE_CLN)?(1):(0))
#endif



typedef struct _tbm_bo_msm
{
    int fd;						/*!< DRM file descriptor for the buffer object */
    int ion_fd;                               /*!< DRM ion_fd */
    unsigned int name;			/*!< DRM buffer name for shared objects */
    int memtype;
    unsigned int size;			/*!< Size of the buffer */
    unsigned int handle;			/*!< Local handle for the buffer */
    int count;					/*!< Number of sub buffers in the object */
    void *hostptr;				/*!< Mapped CPU address for the buffer object */
    void *virtaddr[3];
    unsigned int offsets[3];		 /*!< Array of offsets for the sub buffers */
    unsigned int gpuaddr[3];		/*!< Mapped GPU address for the buffer object */
    int ref;
    int active;					/*!< Currently active sub-buffer in the object */
    unsigned long long offset;	/*!< Mmap() offset for the object */
    int gpu_mapped;                    /*!< Flag if the current bo was mapped */
#ifdef USE_CACHE
    /* for cache control */
    int cache_state;
#endif
#ifdef USE_GENLOCK
	genlock_data_t gLockData;
	genlock_lock_type_t gLockType;
#ifdef GENLOCK_FD
        int opt_vendor_set;
#endif
#endif
}tbm_bo_msm;

typedef struct _tbm_bufmgr_msm
{
    int isLocal;
    int fd;

    void* hashBos;
}tbm_bufmgr_msm;

char* STR_DEVICE[]=
{
    "DEF",
    "CPU",
    "2D",
    "3D",
    "MM"
};

static unsigned int
tbm_msm8974_funcs_bo_export(tbm_bo bo);

#ifdef USE_CACHE
static int
_tbm_msm8974_save_state(tbm_bo_msm *bo_msm);

static int
_tbm_msm8974_bo_cache_init(tbm_bo_msm * bo_msm)
{
    MSM_RETURN_VAL_IF_FAIL (bo_msm != NULL, 0);
    int ret = 0;

    bo_msm->cache_state = TBM_MSM8974_IS_NONE;

    ret = _tbm_msm8974_save_state(bo_msm);

    return ret;
}

static int
_tbm_msm8974_bo_set_data(int fd, uint32_t priv_data, uint32_t handle)
{
    int err;
    struct drm_kgsl_gem_userdata arg;
    int ret = 0;

    arg.priv_data = priv_data;
    arg.handle = handle;
    err = ioctl (fd, DRM_IOCTL_KGSL_GEM_SET_USERDATA, &arg);
    if (err)
    {
#ifdef DEBUG
        DBG("[libtbm-msm8974:%d] "
             "IOCTL  FAILED (%s) %s:%d key:%d data = %d \n",
                getpid(), strerror(errno), __FUNCTION__, __LINE__, handle, priv_data);
#endif
        ret = 0;
    }

#ifdef DEBUG
    DBG("<%s : %d>  fd: %d, key = %d,  data = %d \n", __func__, __LINE__, fd, handle, priv_data);
#endif
    return ret;
}


static int
_tbm_msm8974_bo_get_data(int fd, uint32_t handle)
{
    int err;
    struct drm_kgsl_gem_userdata arg;

    arg.handle = handle;
    err = ioctl (fd, DRM_IOCTL_KGSL_GEM_GET_USERDATA, &arg);
    if (err)
    {
#ifdef DEBUG
        DBG("[libtbm-msm8974:%d] "
             "IOCTL  FAILED (%s) %s:%d key:%d  arg.priv_data= %d  \n",
                getpid(), strerror(errno), __FUNCTION__, __LINE__, handle,arg.priv_data);
#endif
        return 0;
    }
#ifdef DEBUG
    DBG(" <%s : %d>  fd: %d, key = %d  data = %d  \n", __func__, __LINE__, fd, handle,arg.priv_data);
#endif
    return arg.priv_data;
}

static int
_tbm_msm8974_save_state(tbm_bo_msm *bo_msm)
{
    MSM_RETURN_VAL_IF_FAIL (bo_msm!=NULL, -1);
    int ret = 0;
    /* save the access type of the cache/memory */
#ifdef DEBUG
    DBG("<%s : %d>  fd: %d  \n", __func__, __LINE__, bo_msm->fd);
#endif
    ret = _tbm_msm8974_bo_set_data(bo_msm->fd, bo_msm->cache_state ,bo_msm->handle);
    return ret;
}

/**
* @brief Immediate cache operation API (MSM-side)
* @param[in] tbm_bo_msm *bo_msm : the buffer object
* @param[in] int flags { TBM_CACHE_INV=0x01, TBM_CACHE_CLN=0x02 } : Cache flag argument
* @returns cache operation success value
*/

/* TODO: return value needs pairing with libtbm */
void *
tbm_msm8974_bo_cache_flush(tbm_bo bo, int flags)
{

  MSM_RETURN_VAL_IF_FAIL (bo!=NULL, 0);

  int err;
  char need_cache_op = 0;
  struct drm_kgsl_gem_cache_ops cache_op;
  tbm_bo_msm *bo_msm;

  bo_msm = (tbm_bo_msm *)tbm_backend_get_bo_priv(bo);
  MSM_RETURN_VAL_IF_FAIL (bo_msm!=NULL, 0);


  cache_op.handle = bo_msm->handle;
  cache_op.vaddr = bo_msm->hostptr;
  cache_op.length = bo_msm->size;

  //Intializing flags
  cache_op.flags = 0;

  if(flags & TBM_OPTION_VENDOR)
  {
     /* get cache state of a bo */
     bo_msm->cache_state = _tbm_msm8974_bo_get_data (bo_msm->fd, bo_msm->handle);
     if(TBM_MSM8974_IS_CACHE_DIRTY(bo_msm->cache_state))
       need_cache_op = 1;
  }

  if (flags & TBM_CACHE_INV)
  {
   if(flags & TBM_CACHE_CLN)
   {
     bo_msm->cache_state |= TBM_MSM8974_IS_DIRTY;
     cache_op.flags |= DRM_KGSL_GEM_CLEAN_INV_CACHES;
   }
   else
   {
    bo_msm->cache_state |= TBM_MSM8974_IS_INVALID;
    cache_op.flags |= DRM_KGSL_GEM_INV_CACHES;
   }
  }
  else if (flags & TBM_CACHE_CLN)
  {
    bo_msm->cache_state |= TBM_MSM8974_IS_DIRTY;
    cache_op.flags |= DRM_KGSL_GEM_CLEAN_CACHES;
  }

  if(!(flags & TBM_OPTION_VENDOR) || need_cache_op)
  {
     err = ioctl (bo_msm->fd, DRM_IOCTL_KGSL_GEM_CACHE_OPS, &cache_op);

     if (err)
     {
#ifdef DEBUG
       DBG("[libtbm-msm8974:%d] "
          " IOCTL  FAILED (%s) %s:%d key:%d flag : DRM_KGSL_GEM_CACHE_MASK \n",
          getpid(), strerror(errno), __FUNCTION__, __LINE__, bo_msm->handle);
#endif
     }
  }

    /* set data with the appropriate access type bit */
    err = _tbm_msm8974_save_state(bo_msm);
    if (err)
    {
#ifdef DEBUG
       DBG("[libtbm-msm8974:%d]%s:%d key:%d  \n",
          getpid(), __FUNCTION__, __LINE__, bo_msm->handle);
#endif
    }

/* TODO: return value needs pairing with libtbm */
  return NULL;
}

static int
_tbm_msm8974_bo_cache_flush(tbm_bo_msm *bo_msm, int flags)
{
  int err;
  struct drm_kgsl_gem_cache_ops cache_op= {0, };

  cache_op.handle = bo_msm->handle;
  cache_op.vaddr = bo_msm->hostptr;
  cache_op.length = bo_msm->size;


  /* if bo_msm is null, do cache_flush_all */
  if(bo_msm)
  {
    cache_op.handle = bo_msm->handle;
    cache_op.vaddr = bo_msm->hostptr;
    cache_op.length = bo_msm->size;
  }
  else
  {
    flags = TBM_CACHE_FLUSH_ALL;
    cache_op.flags = 0;
    cache_op.vaddr = 0;
    cache_op.length = 0;
  }

  if (flags & TBM_CACHE_INV)
  {
    if (flags & TBM_CACHE_CLN)
      cache_op.flags |= DRM_KGSL_GEM_CLEAN_INV_CACHES;
    else
      cache_op.flags |= DRM_KGSL_GEM_INV_CACHES;
  }
  else if (flags & TBM_CACHE_CLN)
  {
      cache_op.flags |= DRM_KGSL_GEM_CLEAN_CACHES;
  }

#ifdef DEBUG
  DBG("<%s : %d>  fd: %d, cache_op.flags = %d  \n", __func__, __LINE__, bo_msm->fd, cache_op.flags);
#endif

  err = ioctl (bo_msm->fd, DRM_IOCTL_KGSL_GEM_CACHE_OPS, &cache_op);
  if (err)
  {
#ifdef DEBUG
    DBG("[libtbm-msm8974:%d] "
        " IOCTL  FAILED (%s) %s:%d key:%d flag : DRM_KGSL_GEM_CACHE_MASK \n",
        getpid(), strerror(errno), __FUNCTION__, __LINE__, bo_msm->handle);
#endif
    return 0;
  }

  return 1;
}

/* Check if need flush. If needed then fush otherwise return. */
static int
tbm_msm8974_bo_cache_control_op(tbm_bo_msm *bo_msm, int device, int opt)
{
    MSM_RETURN_VAL_IF_FAIL (bo_msm!=NULL, -1);

    char need_cache_op = 0;
    int ret = 0;

    /* get cache state of a bo */
    bo_msm->cache_state = _tbm_msm8974_bo_get_data (bo_msm->fd, bo_msm->handle);

#ifdef DEBUG
    DBG("[libtbm-msm8974:%d]  cache(DIRTY : %d, INV : %d)....flush type :(INVALIDATE : %d , CLEAN : %d )  OPT = %d  \n", getpid(),
        TBM_MSM8974_IS_CACHE_DIRTY(bo_msm->cache_state),
        TBM_MSM8974_IS_CACHE_INV(bo_msm->cache_state),
        TBM_MSM8974_IS_FLUSH_INV(need_cache_op),
        TBM_MSM8974_IS_FLUSH_CLN(need_cache_op),
        opt
        );
#endif

    if (device == TBM_DEVICE_CPU) /* check if CPU accessing cache */
    {
      if((bo_msm->cache_state & TBM_MSM8974_IS_INVALID))
      {
        need_cache_op = TBM_CACHE_INV;
      }

      if (opt & TBM_OPTION_WRITE)
      {
        /* set is dirty */
        bo_msm->cache_state &= ~TBM_MSM8974_IS_INVALID;
        bo_msm->cache_state |= TBM_MSM8974_IS_DIRTY;
      }
    }
    else /* GPU accessing the memory */
    {
      if(bo_msm->cache_state & TBM_MSM8974_IS_DIRTY)
      {
        /* Set the INVALID bit whenever GPU access memory */

        if (opt & TBM_OPTION_WRITE)
        {
          need_cache_op = TBM_CACHE_CLN | TBM_CACHE_INV;
          bo_msm->cache_state &= ~TBM_MSM8974_IS_DIRTY;
          bo_msm->cache_state |= TBM_MSM8974_IS_INVALID;
        }
        else /* READ OPERATION */
        {
          need_cache_op = TBM_CACHE_CLN ;
        }
      }
    }
#ifdef DEBUG
    DBG("\033[31m" "[libtbm-msm8974:%d]  cache(DIRTY : %d, INV : %d)....flush type :(INVALIDATE : %d , CLEAN : %d ) ) \033[0m  \n", getpid(),
        TBM_MSM8974_IS_CACHE_DIRTY(bo_msm->cache_state),
        TBM_MSM8974_IS_CACHE_INV(bo_msm->cache_state),
        TBM_MSM8974_IS_FLUSH_INV(need_cache_op),
        TBM_MSM8974_IS_FLUSH_CLN(need_cache_op)
        );
#endif

    if (need_cache_op)
    {
      /* call backend cache flush */
      _tbm_msm8974_bo_cache_flush(bo_msm, need_cache_op);

      /* set data with the appropriate access type bit */
      ret = _tbm_msm8974_save_state(bo_msm);
    }
    return ret;
}

#endif

static int
tbm_bo_set_memtype(tbm_bo_msm *bo_msm)
{
    int ret;
    struct drm_kgsl_gem_memtype mtype;

    if (bo_msm == NULL || bo_msm->handle == 0)
	return -1;

    memset(&mtype, 0, sizeof(mtype));
    mtype.handle = bo_msm->handle;
    mtype.type = bo_msm->memtype;

    ret = ioctl(bo_msm->fd, DRM_IOCTL_KGSL_GEM_SETMEMTYPE, &mtype);
    if (ret < 0) {
	if (errno == EINVAL) {
	    DBG("DRM:  DRM_IOCTL_KGSL_GEM_SETMEMTYPE is not supported.\n");
	    return -1;
	}
    }

    return ret;
}

int
tbm_bo_get_memtype(tbm_bo_msm *bo_msm)
{
    struct drm_kgsl_gem_memtype mtype;
    int ret;

    if (bo_msm == NULL || bo_msm->handle == 0)
	return -1;

    memset(&mtype, 0, sizeof(mtype));
    mtype.handle = bo_msm->handle;

    ret = ioctl(bo_msm->fd, DRM_IOCTL_KGSL_GEM_GETMEMTYPE, &mtype);

    if (ret < 0)
    {
        DBG("[libtbm-msm8974:%d] %s  DRM_IOCTL_KGSL_GEM_GETMEMTYPE failed. \n", getpid(), __FUNCTION__);
        DBG("[libtbm-msm8974:%d] %s  Reason : %s\n", getpid(), __FUNCTION__, strerror(errno));
        return ret;
    }

    switch(mtype.type)
    {
        case DRM_KGSL_GEM_TYPE_KMEM:
            return MSM_DRM_MEMTYPE_KMEM;
        case DRM_KGSL_GEM_TYPE_KMEM_NOCACHE:
            return MSM_DRM_MEMTYPE_KMEM_NOCACHE;
        case DRM_KGSL_GEM_TYPE_EBI:
            return MSM_DRM_MEMTYPE_EBI;
        case DRM_KGSL_GEM_TYPE_SMI:
            return MSM_DRM_MEMTYPE_SMI;
        case DRM_KGSL_GEM_TYPE_MEM_SECURE:
            return MSM_DRM_MEMTYPE_SECURE;
    }

    return -1;
}

static int
tbm_msm8974_funcs_bo_size(tbm_bo bo)
{
    MSM_RETURN_VAL_IF_FAIL (bo!=NULL, 0);

    tbm_bo_msm *bo_msm;

    bo_msm = (tbm_bo_msm *)tbm_backend_get_bo_priv(bo);

    return bo_msm->size;
}

static void
_tbm_bo_free(tbm_bo_msm *bo_msm)
{
    struct drm_gem_close gemclose;
    int ret=0;
    int i=0;

    if (bo_msm == NULL || bo_msm->handle == 0)
	    return;

    if (bo_msm->hostptr)
    {
        munmap((void *) bo_msm->hostptr, bo_msm->size * bo_msm->count);
        bo_msm->hostptr = NULL;

        for(i = 0; i < bo_msm->count; i++)
            bo_msm->virtaddr[i] = NULL;
    }

    if(bo_msm->gpu_mapped == 1)
    {
        struct drm_kgsl_gem_bind_gpu bind;

        bind.handle = bo_msm->handle;
        ret = ioctl(bo_msm->fd, DRM_IOCTL_KGSL_GEM_UNBIND_GPU, &bind);
        if (ret < 0)
        {
            DBG("[libtbm-msm8974:%d] error %s:%d DRM_IOCTL_KGSL_GEM_UNBIND_GPU failed\n", getpid(), __FUNCTION__, __LINE__);
            DBG("[libtbm-msm8974:%d] %s  Reason : %s\n", getpid(), __FUNCTION__, strerror(errno));
        }

        for(i = 0; i < bo_msm->count; i++)
            bo_msm->gpuaddr[i] = 0;
    }

    /* close ion_fd */
    if (bo_msm->ion_fd > 0)
    {
        close(bo_msm->ion_fd);
        bo_msm->ion_fd = 0;
    }

#ifdef USE_GENLOCK
    if( bo_msm->gLockData.genlockHandle > 0 && bo_msm->gLockData.genlockPrivFd > 0)
    {
        DBG("[PID=%d][%s:%d] genlock_release_lock call. handle=%d privFd=%d\n", getpid(), __FUNCTION__, __LINE__,bo_msm->gLockData.genlockHandle, bo_msm->gLockData.genlockPrivFd);
        if( genlock_release_lock(&bo_msm->gLockData) != GENLOCK_NO_ERROR )
        {
            DBG("[libtbm-msm8974:%d] error %s:%d genlock_release_lock failed\n", getpid(),__FUNCTION__, __LINE__);
            DBG("[libtbm-msm8974:%d] %s  Reason : %s\n", getpid(),__FUNCTION__, strerror(errno));
        }

        bo_msm->gLockData.genlockHandle = 0;
        bo_msm->gLockData.genlockPrivFd = 0;
        bo_msm->opt_vendor_set = 0;
    }
#endif

    memset(&gemclose, 0, sizeof(gemclose));
    gemclose.handle = bo_msm->handle;
    ret = ioctl(bo_msm->fd, DRM_IOCTL_GEM_CLOSE, &gemclose);
    if (ret < 0)
    {
        DBG("[libtbm-msm8974:%d] error %s:%d DRM_IOCTL_GEM_CLOSE failed\n", getpid(), __FUNCTION__, __LINE__);
        DBG("[libtbm-msm8974:%d] %s  Reason : %s\n", getpid(), __FUNCTION__, strerror(errno));
    }

    free(bo_msm);
}

static void
tbm_msm8974_funcs_bo_free(tbm_bo bo)
{
    tbm_bo_msm *bo_msm;

    if(!bo)
        return;

    bo_msm = (tbm_bo_msm *)tbm_backend_get_bo_priv(bo);
    MSM_RETURN_IF_FAIL (bo_msm!=NULL);

#ifdef USE_GENLOCK
    DBG("[libtbm-msm8974:%d][%s:%d] genlock_release_lock will be call. handle=%d privFd=%d flink_id=%d\n", getpid(), __FUNCTION__, __LINE__,bo_msm->gLockData.genlockHandle, bo_msm->gLockData.genlockPrivFd,  tbm_msm8974_funcs_bo_export(bo));
#endif
    _tbm_bo_free(bo_msm);

    return;
}

/*!
 * \brief Allocate the memory associated with the buffer object
 * \param bo the buffer object to allocate
 * \return 0 on success or -1 on failure
 */
static void *
tbm_msm8974_funcs_bo_alloc(tbm_bo bo, int size, int flags)
{
    MSM_RETURN_VAL_IF_FAIL (bo!=NULL, 0);

    struct drm_kgsl_gem_create create;
    struct drm_kgsl_gem_alloc alloc;
    tbm_bo_msm *bo_msm;
    tbm_bufmgr_msm *bufmgr_msm;

#ifdef USE_GENLOCK
    struct drm_kgsl_gem_glockinfo glockinfo;
#endif

    int ret;

    size = (size + (getpagesize() - 1)) & ~(getpagesize() - 1);
    if( size == 0 )
        return NULL;

	// FIXME: Qualcomm ES2 used 32bytes aligned data.
    //size = ALIGN(size,32);

    bufmgr_msm = (tbm_bufmgr_msm *)tbm_backend_get_bufmgr_priv(bo);
    MSM_RETURN_VAL_IF_FAIL (bufmgr_msm!=NULL, 0);

    if (bufmgr_msm->fd == 0)
    {
        DBG("[libtbm-msm8974:%d] error %s:%d drm fd is not exist\n", getpid(), __FUNCTION__, __LINE__);
        return NULL;
    }

    memset(&create, 0, sizeof(create));
    create.size = size;

    ret = ioctl(bufmgr_msm->fd, DRM_IOCTL_KGSL_GEM_CREATE, &create);
    if (ret < 0)
    {
        DBG("[libtbm-msm8974:%d] error %s:%d DRM_IOCTL_KGSL_GEM_CREATE failed\n", getpid(), __FUNCTION__, __LINE__);
        DBG("[libtbm-msm8974:%d] %s  Reason : %s\n", getpid(), __FUNCTION__, strerror(errno));
        return NULL;
    }

    bo_msm = calloc(1, sizeof(tbm_bo_msm));
    if(bo_msm == NULL)
    {
        DBG("[libtbm-msm8974:%d] error %s:%d tbm_bo_msm alloc failed\n", getpid(), __FUNCTION__, __LINE__);
        return NULL;
    }

    bo_msm->size = size;
    bo_msm->handle = create.handle;
    bo_msm->fd = bufmgr_msm->fd;
    bo_msm->active = 0;
    bo_msm->count = 1;
    bo_msm->gpu_mapped = 0;

    if( flags == TBM_BO_VENDOR )
        bo_msm->memtype = MSM_DRM_MEMTYPE_SECURE;
    else if( flags == TBM_BO_NONCACHABLE )
        bo_msm->memtype = MSM_DRM_MEMTYPE_KMEM_NOCACHE;
	else
        bo_msm->memtype = MSM_DRM_MEMTYPE_KMEM;

    DBG("[libtbm-msm8974:%d] %s  Set memtype = %d\n", getpid(), __FUNCTION__, bo_msm->memtype);
    tbm_bo_set_memtype(bo_msm);

    memset(&alloc, 0, sizeof(alloc));
    alloc.handle = bo_msm->handle;

    ret = ioctl(bo_msm->fd, DRM_IOCTL_KGSL_GEM_ALLOC, &alloc);
    if (ret < 0)
    {
        DBG("[libtbm-msm8974:%d] %s  failed to DRM_IOCTL_KGSL_GEM_ALLOC\n", getpid(), __FUNCTION__);
        DBG("[libtbm-msm8974:%d] %s  Reason : %s\n", getpid(), __FUNCTION__, strerror(errno));
        free (bo_msm);
        return NULL;
    }

    bo_msm->offset = alloc.offset;

    if( bo_msm->ion_fd  == 0 )
    {
        struct drm_kgsl_gem_get_ion_fd ionfdioctl;

        ionfdioctl.handle = bo_msm->handle;
        if (ioctl (bo_msm->fd, DRM_IOCTL_KGSL_GEM_GET_ION_FD, &ionfdioctl) )
        {
            DBG ("[libtbm-msm8x30:%d] error %s:%d Cannot ionfd=%d\n", getpid(), __FUNCTION__, __LINE__, bo_msm->handle);
            DBG("[libtbm-msm8974:%d] %s  Reason : %s\n", getpid(), __FUNCTION__, strerror(errno));
			free (bo_msm);
            return NULL;
        }
        bo_msm->ion_fd = ionfdioctl.ion_fd;
    }

#ifdef USE_GENLOCK
    bo_msm->gLockData.genlockPrivFd = 0;
    bo_msm->gLockData.genlockHandle = 0;
    bo_msm->opt_vendor_set = 0;

    if( genlock_create_lock(&bo_msm->gLockData)!= GENLOCK_NO_ERROR)
    {
        DBG (" %s:%d GENLOCK CREATE LOCK failed\n", __FUNCTION__, __LINE__);
        DBG(" %s  Reason : %s\n", __FUNCTION__, strerror(errno));
        free (bo_msm);
        return NULL;
    }
    else
    {
      DBG (" %s:%d genlock_create_lock is success. lockfd=%d, handle=%d\n",
          __FUNCTION__, __LINE__,
          bo_msm->gLockData.genlockPrivFd, bo_msm->gLockData.genlockHandle);
      glockinfo.handle = bo_msm->handle;
      glockinfo.glockhandle[0] = bo_msm->gLockData.genlockPrivFd;
      ret = ioctl(bo_msm->fd, DRM_IOCTL_KGSL_GEM_SET_GLOCK_HANDLES_INFO, &glockinfo);
      if( ret < 0 )
      {
        DBG (" %s:%d GEM_SET_GLOCK_HANDLES_INFO failed\n", __FUNCTION__, __LINE__);
        DBG(" %s  Reason : %s\n", __FUNCTION__, strerror(errno));
      }
    }
        DBG("  [libtbm-msm8974:< %s : %d >  genlockPrivFd = %d \n",  __FUNCTION__, __LINE__, bo_msm->gLockData.genlockPrivFd);
#endif


#ifdef USE_CACHE
    /***************** Init Cache ops*************************/
    _tbm_msm8974_bo_cache_init(bo_msm);
#endif

    DBG("[libtbm-msm8974:%d] %s  Success!!\n", getpid(), __FUNCTION__);

    return (void *)bo_msm;
}

/*!
 * \brief Import to a DRM buffer name and create a local handle
 * \param fd File descriptor for the DRM device
 * \param name The DRM buffer name to attach to
 * \return A pointer to the local buffer object structure or NULL on error
 */
static void *
tbm_msm8974_funcs_bo_import(tbm_bo bo, unsigned int key)
{
    MSM_RETURN_VAL_IF_FAIL (bo!=NULL, 0);

    struct drm_gem_open open;
    tbm_bo_msm *bo_msm;
    tbm_bufmgr_msm *bufmgr_msm;

#ifdef USE_GENLOCK
    struct drm_kgsl_gem_glockinfo glockinfo;
#endif

    int ret;

    bufmgr_msm = (tbm_bufmgr_msm *)tbm_backend_get_bufmgr_priv(bo);
    MSM_RETURN_VAL_IF_FAIL (bufmgr_msm!=NULL, 0);

    if (bufmgr_msm->fd == 0)
    {
        DBG("[libtbm-msm8974:%d] error %s:%d drm fd is not exist\n", getpid(), __FUNCTION__, __LINE__);
        return NULL;
    }

    bo_msm = calloc(1, sizeof(tbm_bo_msm));
    if (bo_msm == NULL)
    {
        DBG("[libtbm-msm8974:%d] error %s:%d tbm_bo_msm alloc failed\n", getpid(), __FUNCTION__, __LINE__);
        return NULL;
    }

    memset(&open, 0, sizeof(open));
    open.name = key;

    ret = ioctl(bufmgr_msm->fd, DRM_IOCTL_GEM_OPEN, &open);
    if (ret < 0)
    {
        DBG("[libtbm-msm8974:%d] error %s:%d DRM_IOCTL_GEM_OPEN failed\n", getpid(), __FUNCTION__, __LINE__);
        DBG("[libtbm-msm8974:%d] %s  Reason : %s\n", getpid(), __FUNCTION__, strerror(errno));
        free (bo_msm);
        return NULL;
    }

    bo_msm->size = open.size;
    bo_msm->handle = open.handle;
    bo_msm->fd = bufmgr_msm->fd;
    bo_msm->name = key;

    bo_msm->count = -1;
    bo_msm->offset = 0;
    bo_msm->memtype = -1;
    bo_msm->gpu_mapped = 0;

    bo_msm->memtype = tbm_bo_get_memtype(bo_msm);
    DBG("[libtbm-msm8974:%d] %s  Get memtype = %d\n", getpid(), __FUNCTION__, bo_msm->memtype);

    if( bo_msm->ion_fd  == 0 )
    {
        struct drm_kgsl_gem_get_ion_fd ionfdioctl;

        ionfdioctl.handle = bo_msm->handle;
        if (ioctl (bo_msm->fd, DRM_IOCTL_KGSL_GEM_GET_ION_FD, &ionfdioctl) )
        {
            DBG ("[libtbm-msm8x30:%d] error %s:%d Cannot ionfd=%d\n", getpid(), __FUNCTION__, __LINE__, bo_msm->handle);
            DBG("[libtbm-msm8974:%d] %s  Reason : %s\n", getpid(), __FUNCTION__, strerror(errno));
			free (bo_msm);
            return NULL;
        }
        bo_msm->ion_fd = ionfdioctl.ion_fd;
    }

#ifdef USE_GENLOCK
    memset(&glockinfo, 0, sizeof(glockinfo));
    glockinfo.handle = bo_msm->handle;

    if(ioctl(bo_msm->fd, DRM_IOCTL_KGSL_GEM_GET_GLOCK_HANDLES_INFO, &glockinfo))
    {
        DBG (" %s:%d GET_GLOCK_HANDLES_INFO failed \n",  __FUNCTION__, __LINE__);
        DBG(" %s  Reason : %s\n",  __FUNCTION__, strerror(errno));
        free (bo_msm);
        return NULL;
    }

    // FIXME: glockinfo.glockhandle is array type? Should be check!
    bo_msm->gLockData.genlockHandle = glockinfo.glockhandle[0];
    if(genlock_attach_lock(&bo_msm->gLockData) != GENLOCK_NO_ERROR)
    {
        DBG (" %s:%d genlock_attach_lock failed \n",  __FUNCTION__, __LINE__);
        DBG(" %s genlockPrivFd=%d genlockhandle=%d  Reason : %s\n",  __FUNCTION__, bo_msm->gLockData.genlockPrivFd, bo_msm->gLockData.genlockHandle, strerror(errno));
        free (bo_msm);
        return NULL;
    }
    else
    {
        DBG (" %s:%d GENLOCK genlock_attach_lock is success! lockfd=%d, handle=%d\n",
				 __FUNCTION__, __LINE__,
				bo_msm->gLockData.genlockPrivFd, bo_msm->gLockData.genlockHandle);
    }
#endif

    DBG("[libtbm-msm8974:%d][%s] buffer->name=%d is imported!\n", getpid(), __FUNCTION__, key);
    return (void *)bo_msm;
}

static unsigned int
tbm_msm8974_funcs_bo_export(tbm_bo bo)
{
    MSM_RETURN_VAL_IF_FAIL (bo!=NULL, 0);

    tbm_bo_msm *bo_msm;
    struct drm_gem_flink flink;
    int ret;

    bo_msm = (tbm_bo_msm *)tbm_backend_get_bo_priv(bo);
    MSM_RETURN_VAL_IF_FAIL (bo_msm!=NULL, 0);

    if( bo_msm->name == 0 )
    {
        memset(&flink, 0, sizeof(flink));

        flink.handle = bo_msm->handle;
        ret = ioctl(bo_msm->fd, DRM_IOCTL_GEM_FLINK, &flink);
        if (ret < 0)
        {
            DBG("[libtbm-msm8974:%d] error %s:%d DRM_IOCTL_GEM_FLINK failed\n", getpid(), __FUNCTION__, __LINE__);
            DBG("[libtbm-msm8974:%d] %s  Reason : %s\n", getpid(), __FUNCTION__, strerror(errno));
            return -1;
        }

        bo_msm->name = flink.name;
    }

    return (unsigned int)bo_msm->name;
}

static int _tbm_msm8974_funcs_bo_map_cpu(tbm_bo_msm *bo_msm)
{
    int i = 0;
    int ret = 0;
    unsigned int mapsize = 0;

    mapsize = bo_msm->size;
    bo_msm->offsets[0] = 0;

    /* Already mapped */
    if (bo_msm->hostptr != NULL)
    {
        DBG("[libtbm-msm8974:%d] %s:%d bo_msm->hostptr is already mapped\n", getpid(), __FUNCTION__, __LINE__);
        return 1;
    }

#ifdef DRM_IOCTL_KGSL_GEM_GET_BUFINFO
	struct drm_kgsl_gem_bufinfo bufinfo;
	bufinfo.handle = bo_msm->handle;

	ret = ioctl(bo_msm->fd, DRM_IOCTL_KGSL_GEM_GET_BUFINFO, &bufinfo);

	if (ret == 0) {
	    for(i = 0; i < bufinfo.count; i++) {
		bo_msm->offsets[i] = bufinfo.offset[i];
		bo_msm->gpuaddr[i] = bufinfo.gpuaddr[i];
	    }

	    bo_msm->count = bufinfo.count;
	    bo_msm->active = bufinfo.active;

	    mapsize = bo_msm->size * bo_msm->count;
	}
    DBG("[libtbm-msm8974:%d] %s:%d buffer->name=%d count=%d active=%d\n", getpid(), __FUNCTION__, __LINE__, bo_msm->name, bo_msm->count, bo_msm->active);
#endif

    if (ret == 0)
    {
        bo_msm->hostptr = mmap(0, mapsize, PROT_READ | PROT_WRITE, MAP_SHARED, bo_msm->ion_fd, 0);
    }
    else
    {
        DBG("[libtbm-msm8974:%d] error %s:%d DRM: DRM_IOCTL_KGSL_GEM_GET_BUFINFO failed:%m\n", getpid(), __FUNCTION__, __LINE__);
        return -1;
    }

    if (bo_msm->hostptr == MAP_FAILED) {
        DBG("[libtbm-msm8974:%d] error %s:%d DRM:  Unable to map: %m\n", getpid(), __FUNCTION__, __LINE__);
        return -1;
    }

    for(i = 0; i < bo_msm->count; i++)
	bo_msm->virtaddr[i] = (void *) bo_msm->hostptr + bo_msm->offsets[i];

    return ret;
}

static int _tbm_msm8974_funcs_bo_map_gpu(tbm_bo_msm *bo_msm)
{
    struct drm_kgsl_gem_bind_gpu bind;
    int ret;

    if (bo_msm->gpuaddr[0])
    {
        DBG("[libtbm-msm8974:%d] %s:%d bo_msm->gpuaddr is already mapped\n", getpid(), __FUNCTION__, __LINE__);
        return 1;
    }
    bind.handle = bo_msm->handle;

    ret = ioctl(bo_msm->fd, DRM_IOCTL_KGSL_GEM_BIND_GPU, &bind);
    if (ret < 0)
    {
        DBG("[libtbm-msm8974:%d] error %s:%d DRM_IOCTL_KGSL_GEM_BIND_GPU\n", getpid(), __FUNCTION__, __LINE__);
        DBG("[libtbm-msm8974:%d] %s  Reason : %s\n", getpid(), __FUNCTION__, strerror(errno));
        return ret;
    }
    bo_msm->gpu_mapped = 1;

	// FIXME: After GPU_BIND, is bind.gpuptr same as bufinfo.gpuaddr[i]??
    bo_msm->gpuaddr[0] = bind.gpuptr;

#ifdef DRM_IOCTL_KGSL_GEM_GET_BUFINFO
	int i;
	struct drm_kgsl_gem_bufinfo bufinfo;
	bufinfo.handle = bo_msm->handle;

	ret = ioctl(bo_msm->fd, DRM_IOCTL_KGSL_GEM_GET_BUFINFO, &bufinfo);

	for(i = 0; !ret && i < bufinfo.count; i++)
	{
	    bo_msm->gpuaddr[i] = bufinfo.gpuaddr[i];
        bo_msm->offsets[i] = bufinfo.offset[i];
    }
    DBG("[libtbm-msm8974:%d] %s:%d bo_msm->count=%d active=%d\n", getpid(), __FUNCTION__, __LINE__, bo_msm->count, bo_msm->active);
    bo_msm->count = bufinfo.count;
    bo_msm->active = bufinfo.active;
#endif



    /* If the ioctls are in place, then get the buffer info to get the GPU
       addresses directly */

    return 1;
}

/**
 *  In KGSL-DRM,
 *      TBM_DEVICE_DEFAULT      : return gem handle
 *      TBM_DEVICE_CPU             : return virtual address
 *      TBM_DEVICE_2D/MM         : return ion_fd
 *      TBM_DEVICE_3D               : return gpu address. ( This is physical address )
 */
static tbm_bo_handle
tbm_msm8974_funcs_bo_map(tbm_bo bo, int device, int opt)
{
  MSM_RETURN_VAL_IF_FAIL (bo!=NULL, (tbm_bo_handle) NULL);

  tbm_bo_msm *bo_msm;
  tbm_bo_handle ret;

  bo_msm = (tbm_bo_msm *)tbm_backend_get_bo_priv(bo);
  MSM_RETURN_VAL_IF_FAIL (bo_msm!=NULL, (tbm_bo_handle) NULL);

  if( bo_msm->memtype == MSM_DRM_MEMTYPE_SECURE )
  {
    if( device == TBM_DEVICE_CPU || device == TBM_DEVICE_3D )
    {
      DBG("[libtbm-msm8974:%d] %s:%d MSM_DRM_MEMTYPE_SECURE is not support %s device\n", getpid(), __FUNCTION__, __LINE__, STR_DEVICE[device]);
      return (tbm_bo_handle) NULL;
    }
  }

  if( device == TBM_DEVICE_CPU )
  {
    _tbm_msm8974_funcs_bo_map_cpu(bo_msm);
    // FIXME: need to verify bo_msm->active!!
    ret.ptr = bo_msm->virtaddr[bo_msm->active];;
  }
  else if( device == TBM_DEVICE_3D )
  {
#ifdef USE_GENLOCK
    if(opt == TBM_OPTION_VENDOR)
    {
      DBG("  [libtbm-msm8974:< %s : %d >  genlockPrivFd = %d \n",  __FUNCTION__, __LINE__, bo_msm->gLockData.genlockPrivFd);
      ret.s32 = (uint32_t)bo_msm->gLockData.genlockPrivFd;
      DBG("tbm_msm8974_funcs_bo_map]   bo_msm->opt_vendor_set = %d ,  genlockPrivFd = %d ", bo_msm->opt_vendor_set,bo_msm->gLockData.genlockPrivFd );
    }
    else
    {
      _tbm_msm8974_funcs_bo_map_gpu(bo_msm);
      // FIXME: need to verify bo_msm->active!!
      ret.u32 = (uint32_t)bo_msm->gpuaddr[bo_msm->active];
    }
#else
    _tbm_msm8974_funcs_bo_map_gpu(bo_msm);
    // FIXME: need to verify bo_msm->active!!
    ret.u32 = (uint32_t)bo_msm->gpuaddr[bo_msm->active];
#endif
  }
  else if( device == TBM_DEVICE_2D || device == TBM_DEVICE_MM )
  {
    ret.s32 = (uint32_t)bo_msm->ion_fd;
  }
  else if( device == TBM_DEVICE_DEFAULT )
  {
    ret.u32 = (uint32_t)bo_msm->handle;
  }
  else
  {
    DBG("[libtbm-msm8974:%d] error %s:%d Device(%s) is not supported in tbm_msm\n", getpid(), __FUNCTION__, __LINE__, STR_DEVICE[device]);
    return (tbm_bo_handle) NULL;
  }

  DBG("[libtbm-msm8974:%d] %s:%d device_type=%d bo_msm->count=%d active=%d\n", getpid(), __FUNCTION__, __LINE__, device, bo_msm->count, bo_msm->active);

#ifdef USE_CACHE
    /* Perform Cache Control Operationh */
    if(opt)
      tbm_msm8974_bo_cache_control_op(bo_msm, device, opt);
#endif

    return ret;
}

static tbm_bo_handle
tbm_msm8974_funcs_bo_get_handle(tbm_bo bo, int device)
{
    MSM_RETURN_VAL_IF_FAIL (bo!=NULL, (tbm_bo_handle) NULL);

    tbm_bo_handle ret=(tbm_bo_handle)NULL;

    ret = tbm_msm8974_funcs_bo_map(bo, device, 0);

    return ret;
}

static int
tbm_msm8974_funcs_bo_unmap(tbm_bo bo)
{
    /* To be implemented .. */
    tbm_bo_msm *bo_msm = NULL;
    int ret = 0;
    bo_msm = (tbm_bo_msm *)tbm_backend_get_bo_priv(bo);
    MSM_RETURN_VAL_IF_FAIL (bo_msm!=NULL, 0);

#ifdef USE_CACHE
    /* update the cache state of the bo */
    ret = _tbm_msm8974_save_state(bo_msm);
#endif

    return ret;
}

static int
tbm_msm8974_funcs_bo_get_global_key(tbm_bo bo)
{
    MSM_RETURN_VAL_IF_FAIL (bo!=NULL, 0);

    tbm_bo_msm *bo_msm;

    bo_msm = (tbm_bo_msm *)tbm_backend_get_bo_priv(bo);
    MSM_RETURN_VAL_IF_FAIL (bo_msm!=NULL, 0);

    if (!bo_msm->name)
    {
        if (!bo_msm->fd)
            return -1;

        bo_msm->name = tbm_msm8974_funcs_bo_export(bo);
    }

    return bo_msm->name;
}

static int
tbm_msm8974_bo_lock(tbm_bo bo, int device, int option)
{
#ifdef USE_GENLOCK
    MSM_RETURN_VAL_IF_FAIL (bo!=NULL, -1);

    tbm_bo_msm *bo_msm;
    genlock_lock_type_t gLockType=0;

    if( option == TBM_OPTION_READ || option ==  TBM_OPTION_VENDOR )
        gLockType = GENLOCK_READ_LOCK;
    else
        gLockType = GENLOCK_WRITE_LOCK;

    bo_msm = (tbm_bo_msm *)tbm_backend_get_bo_priv(bo);
    MSM_RETURN_VAL_IF_FAIL (bo_msm!=NULL, -1);

    if( bo_msm->gLockData.genlockHandle <= 0 || bo_msm->gLockData.genlockPrivFd <= 0 )
    {
        DBG(" error: bo_msm->gLockData is not initialied\n");
        return -1;
    }

    DBG (" %s:%d GEN TRY LOCK flink_id=%d lockfd=%d, handle=%d\n",
    		 __FUNCTION__, __LINE__, bo_msm->name,
    		bo_msm->gLockData.genlockPrivFd, bo_msm->gLockData.genlockHandle);
    if( genlock_lock_buffer(&bo_msm->gLockData, gLockType, GENLOCK_MAX_TIMEOUT)!= GENLOCK_NO_ERROR)
    {
        DBG (" %s:%d Unable to take [%d] lock. flink id=%d lockfd=%d lockhandle=%d\n",
    			 __FUNCTION__, __LINE__, gLockType,
    			bo_msm->name, bo_msm->gLockData.genlockPrivFd, bo_msm->gLockData.genlockHandle);
        DBG(" %s  Reason : %s\n",  __FUNCTION__, strerror(errno));
        return -1;
    }
    else
    {
        DBG (" %s:%d GEN LOCK flink_id=%d lockfd=%d, handle=%d\n",
    			 __FUNCTION__, __LINE__, bo_msm->name,
    			bo_msm->gLockData.genlockPrivFd, bo_msm->gLockData.genlockHandle);
#ifdef GENLOCK_FD

        if( option ==  TBM_OPTION_VENDOR )
           bo_msm->opt_vendor_set += 1;
#endif
    }
#endif
	return 1;
}

static int
tbm_msm8974_bo_unlock(tbm_bo bo)
{
#ifdef USE_GENLOCK
    MSM_RETURN_VAL_IF_FAIL (bo!=NULL, -1);

    tbm_bo_msm *bo_msm;

    bo_msm = (tbm_bo_msm *)tbm_backend_get_bo_priv(bo);
    MSM_RETURN_VAL_IF_FAIL (bo_msm!=NULL, -1);

    if( bo_msm->gLockData.genlockHandle <= 0 || bo_msm->gLockData.genlockPrivFd <= 0 )
    {
        DBG(" error: bo_msm->gLockData is not initialied\n");
        return -1;
    }
#ifdef USE_GENLOCK
#ifdef GENLOCK_FD
    if (bo_msm->opt_vendor_set > 0)
    {
      DBG(" error: bo_msm->gLockData.opt_vendor_set is set. Unlock shall be done by egl core.\n");
      //Resetting the vendor flag
      bo_msm->opt_vendor_set -= 1;

      //No unlock operation. Sending Success.
      return 1;

    }
#endif
#endif

    DBG (" %s:%d GEN TRY UNLOCK flink_id=%d lockfd=%d, handle=%d\n",
			 __FUNCTION__, __LINE__, bo_msm->name,
			bo_msm->gLockData.genlockPrivFd, bo_msm->gLockData.genlockHandle);
    if( genlock_unlock_buffer(&bo_msm->gLockData)!= GENLOCK_NO_ERROR)
    {
        DBG (" %s:%d Unable to unlock. flink id=%d lockfd=%d lockhandle=%d\n",
				 __FUNCTION__, __LINE__,
				bo_msm->name, bo_msm->gLockData.genlockPrivFd, bo_msm->gLockData.genlockHandle);
        DBG(" %s  Reason : %s\n",  __FUNCTION__, strerror(errno));
        return -1;
    }
    else
    {
        DBG (" %s:%d GEN UNLOCK flink_id=%d lockfd=%d, handle=%d\n",
				 __FUNCTION__, __LINE__, bo_msm->name,
				bo_msm->gLockData.genlockPrivFd, bo_msm->gLockData.genlockHandle);
    }
#endif

	return 1;
}

static void
tbm_msm8974_funcs_bufmgr_deinit(void *priv)
{
    MSM_RETURN_IF_FAIL (priv!=NULL);

    tbm_bufmgr_msm *bufmgr_msm = (tbm_bufmgr_msm *)priv;

    if(bufmgr_msm->hashBos)
    {
        unsigned long key;
        void *value;

        while(0 < drmHashFirst(bufmgr_msm->hashBos, &key, &value))
        {
            free(value);
            drmHashDelete(bufmgr_msm->hashBos, key);
        }

        drmHashDestroy(bufmgr_msm->hashBos);
    }

    free(bufmgr_msm);
}


MODULEINITPPROTO (init_tbm_bufmgr_priv);

static TBMModuleVersionInfo MSM8974VersRec =
{
    "MSM8974",
    "Samsung",
    TBM_ABI_VERSION,
};

TBMModuleData tbmModuleData = { &MSM8974VersRec, init_tbm_bufmgr_priv};

int
init_tbm_bufmgr_priv(tbm_bufmgr bufmgr, int fd)
{
    tbm_bufmgr_msm *bufmgr_msm;
    tbm_bufmgr_backend bufmgr_backend;

    if(!bufmgr)
        return 0;

    bufmgr_msm = calloc(1, sizeof(tbm_bufmgr_msm));
    if(!bufmgr_msm)
    {
        DBG("[libtbm-msm8974:%d] error: Fail to alloc bufmgr_msm!\n", getpid());
        return 0;
    }

    //Create Hash Table
    bufmgr_msm->hashBos = drmHashCreate();

    bufmgr_msm->fd = fd;

    bufmgr_backend = tbm_backend_alloc();
    if (!bufmgr_backend)
    {
        fprintf (stderr, "[libtbm-msm:%d] error: Fail to create drm!\n", getpid());
        free (bufmgr_msm);
        return 0;
    }

    bufmgr_backend->flags = TBM_CACHE_CTRL_BACKEND|TBM_LOCK_CTRL_BACKEND;

    bufmgr_backend->priv = (void *)bufmgr_msm;
    bufmgr_backend->bufmgr_deinit = tbm_msm8974_funcs_bufmgr_deinit,
    bufmgr_backend->bo_size = tbm_msm8974_funcs_bo_size,
    bufmgr_backend->bo_alloc = tbm_msm8974_funcs_bo_alloc,
    bufmgr_backend->bo_free = tbm_msm8974_funcs_bo_free,
    bufmgr_backend->bo_import = tbm_msm8974_funcs_bo_import,
    bufmgr_backend->bo_export = tbm_msm8974_funcs_bo_export,
    bufmgr_backend->bo_get_handle = tbm_msm8974_funcs_bo_get_handle,
    bufmgr_backend->bo_map = tbm_msm8974_funcs_bo_map,
    bufmgr_backend->bo_unmap = tbm_msm8974_funcs_bo_unmap,
/* TODO: return value needs pairing with libtbm */
    bufmgr_backend->bo_cache_flush = (void *)tbm_msm8974_bo_cache_flush,
    bufmgr_backend->bo_get_global_key = tbm_msm8974_funcs_bo_get_global_key,
    bufmgr_backend->bo_lock = NULL;
    bufmgr_backend->bo_lock2 = tbm_msm8974_bo_lock; /* bo_lock2 has device and opt parameter */
    bufmgr_backend->bo_unlock = tbm_msm8974_bo_unlock;

    if (!tbm_backend_init (bufmgr, bufmgr_backend))
    {
        fprintf (stderr, "[libtbm-msm:%d] error: Fail to init backend!\n", getpid());
        tbm_backend_free (bufmgr_backend);
        free (bufmgr_msm);
        return 0;
    }

#ifdef DEBUG
    {
        char* env;
        env = getenv("TBM_MSM8974_DEBUG");
        if(env)
        {
            bDebug = atoi(env);
            fprintf(stderr, "TBM_MSM8974_DEBUG=%s\n", env);
        }
        else
        {
            bDebug = 0;
        }
    }
#endif

    DBG("[libtbm-msm8974:%d] %s fd:%d\n", getpid(), __FUNCTION__, fd);

    return 1;
}

